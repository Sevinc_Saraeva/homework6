
import java.util.Arrays;
import java.util.Random;


public class Human {
    private String name;
    private String surname;
    private  int year;
    private  int iq_level;
    private  Family family;
    private String[][] shedule;

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }
    public Human(String name, String surname, int year , Family family) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.family = family;
    }


    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public Human() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq_level() {
        return iq_level;
    }

    public void setIq_level(int iq_level) {
        this.iq_level = iq_level;
    }





    public String[][] getShedule() {
        return shedule;
    }

    public void setShedule(String[][] shedule) {
        shedule = shedule;
    }



    public Human(String name, String surname, int year, int iq_level, String[][] shedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq_level = iq_level;

        this.shedule = shedule;
    }
    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq_level=" + iq_level +
                ", schedule= " + Arrays.toString(shedule)+
                '}';
    }

    public void greetPet(){
        System.out.println("Hello, " + this.family.getPet().getNickname());

    }
    public void describePet(){
        System.out.print("I have " + this.family.getPet().getSpecies() + ", he is " + this.family.getPet().getAge() + " years old. ");
        if(this.family.getPet().getTrickLevel()<50) System.out.println("He is almost not sly");
        else System.out.println("He is sly");
    }
    @Override
    protected void finalize() throws Throwable {
        System.out.println("Finalize method called in Human class");
        super.finalize();
    }
    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof Human))
            return false;
        if (obj == this)
            return true;
        return this.getName() == ((Human) obj).getName() && this.getSurname() == ((Human) obj).getSurname();
    }



//    public boolean feedPet(boolean timefeed) {
//        if (timefeed) {
//            feedPetAndMessage(timefeed);
//            return true;
//        }
//        Random random = new Random();
//        int rand = random.nextInt(100 - 1 + 1) + 1;
//        if (rand > this.pet.getTrickLevel()) {
//            feedPetAndMessage(true);
//            return true;
//        } else {
//            System.out.println("I think Jack is not hungry.");
//            return false;
//        }
//    }
//    public boolean feedPetAndMessage(boolean b){
//        System.out.println("Hm... I will feed Jack's " + pet.getNickname());
//        return true;
//    }
}

