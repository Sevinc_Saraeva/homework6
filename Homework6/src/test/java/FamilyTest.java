import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;



class FamilyTest {

    Family family;
    Family family2;
    Family family3;
    Human [] children;
    Human child2;
    Human child1;
    Human testChild;
    Pet pet;
    Pet pet2;
    Pet pet3;
    Human mother;
    Human father;
    @BeforeEach
    public void init() {
        mother = new Human("A", "AA", 1959);
        father = new Human("B", "BB", 1959);
        family = new Family(mother, father);
         child1 = new Human("C", "AA", 2000);
         child2 = new Human("D", "AA", 2000);
        children = new Human[]{child1, child2};
        family.setChildren(children);
        testChild = new Human("Z", "ZZ", 2000);

        family2 = new Family(new Human("A", "AA", 1959),
                new Human("B", "BB", 1959));
        pet = new Pet(Species.FISH, "fish");

    }

   @Test
    public void deleteChildWithIndex(){
        assertTrue(family.deleteChild(1));

   }
    @Test
    public void deleteChildWithWrongIndex(){
        assertFalse(family.deleteChild(-1));

    }

    @Test
   public void  deleteChildWithObject(){
        assertTrue(family.deleteChild(child1));
   }

    @Test
    public void  deleteChildWithWrongObject(){
        assertFalse(family.deleteChild(testChild));
    }
    @Test
    public void  addChild(){
        // before adding child array's length
       int x = family.getChildren().length;
       // adding child
        family.addChild(testChild);
        // after adding child array's length must be equals to x++;
        x++;
        // check condition
        assertTrue(x==family.getChildren().length &&
                family.getChildren()[family.getChildren().length-1].equals(testChild));

    }
    @Test
    public void countFamily(){
        int expected = 2+family.getChildren().length;
        assertEquals(expected, family.countFamily());
    }
@Test
    public void testEqualsTrueHuman(){
        Human testChild = new Human(child1.getName(), child1.getSurname(), child1.getYear());
        assertEquals(true, child1.equals(testChild));
    }
    @Test
    public void testEqualsFalseHuman(){
        assertEquals(false,child1.equals(child2));
    }
    @Test
    public void testEqualsTrueFamily(){
        family2 = new Family(new Human(mother.getName(), mother.getSurname(), mother.getYear()),
                new Human(father.getName(), father.getSurname(), father.getYear()));
        family2.setChildren(children);
        assertEquals(true,family.equals(family2));
    }
    @Test
    public void testEqualsFalseFamily(){
        family3 = new Family(new Human("B", "QQ", 1959),
                new Human("W", "EE", 1959));
        assertEquals(false,family.equals(family3));
    }
    @Test
public  void  testPetTrue(){
    pet2 = new Pet(pet.getSpecies(), pet.getNickname());
    assertEquals(true, pet.equals(pet2));
}



    }